#!/usr/bin/env python
#       File name: siem_connector.py
#       Author: Jakub Toczek (PSNC)
#       Date created: 30/10/2017
#       Python Version: 2.7.13
from warden_client import Client, read_cfg
import datetime
from time import time
import json
import sys
import os
import uuid
import subprocess

DEFAULT_ACONFIG = 'warden_client-siem.cfg'
DEFAULT_WCONFIG = 'warden_client.cfg'
DEFAULT_NAME = 'org.example.warden.test'
DEFAULT_AWIN = 5
DEFAULT_ANONYMISED = 'no'
DEFAULT_TARGET_NET = '0.0.0.0/0'
DEFAULT_SECRET = ''

logfile = "/var/log/siem.log"
last_run_file = "last_run.log"

def write_common_information(dictionary, event_id, dateFormat, severity):
    dictionary.update({'Format': 'IDEA0'})
    dictionary.update({'ID': event_id})
    dictionary.update({'DetectTime': str(dateFormat.strftime('%Y-%m-%dT%H:%M:%SZ'))})
    dictionary.update({'CreateTime': str(datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'))})
    dictionary.update({'Category': severity})


def getIdeaLog(line, anonymised, target_net, aname):
    dictionary = {}
    words = line.split()
    jsonData = json.loads(" ".join(words[5:]))
    event_id = str(uuid.uuid4())
    dateFormat = datetime.datetime.strptime(jsonData["data"]["firsttime"], '%Y-%m-%dT%H:%M:%SZ')

    write_common_information(dictionary, event_id, dateFormat, [str(jsonData["data"]["total_severity"])])
    write_sources_targets(dictionary, jsonData["data"], anonymised, target_net)

    write_node(dictionary, jsonData, words[4], aname)
    return dictionary

def write_sources_targets(dictionary, jsonData, anonymised, target_net):

    source_dictionary = {}
    if ":" in jsonData["src_ip"]:
        ip = 'IP6'
    else:
        ip = 'IP4'
    source_dictionary.update({ip: [jsonData["src_ip"]]})
    source_dictionary.update({'MAC': [jsonData["src_mac"]]})
    source_dictionary.update({'Proto': [jsonData["protocol"]]})
    source_dictionary.update({'Port': [jsonData["src_port"]]})
    dictionary.update({'Source': [source_dictionary]})

    target_dictionary = {}
    if ":" in jsonData["dst_ip"]:
        ip = 'IP6'
    else:
        ip = 'IP4'
    if anonymised != 'omit':
        if anonymised == 'yes':
            target_dictionary.update({'Anonymised':True})
            target_dictionary.update({ip: [target_net]})
        else:
            target_dictionary.update({ip: [jsonData["dst_ip"]]})
    target_dictionary.update({'MAC': [jsonData["dst_mac"]]})
    target_dictionary.update({'Proto': [jsonData["protocol"]]})
    target_dictionary.update({'Port': [jsonData["dst_port"]]})
    dictionary.update({'Target': [target_dictionary]}) 


def write_node(dictionary, jsonData, type, aname):
    node_dictionary = {}
    node_dictionary.update({'Name': aname})
    node_dictionary.update({'SW': [jsonData["source"]["name"].replace(" ", "_")]})
    node_dictionary.update({'Type': [type[:-1]]})
    dictionary.update({'Node': [node_dictionary]})

def create_output_name(path, i):
    return path + 'out' + str(i)

def get_last_log(logfile):
    line = subprocess.check_output(['tail', '-1', logfile])[0:-1]
    return line
def parseFile(logfile, aanonymised, atargetnet, aname):
    flag = 0
    last_log_time = get_last_log(logfile).split()[0:3]
    first_log_time = open(logfile, 'r').readline().split()[0:3]
    last_script_run = []
    if os.path.exists(last_run_file):
        with open(last_run_file, 'r') as f:
            last_script_run = f.readline().split()
    else:
        open(last_run_file,"a+")
    ideaLogs = []
    if last_log_time != last_script_run:
        if not last_script_run or datetime.datetime.strptime(' '.join(first_log_time),'%b %d %H:%M:%S')>datetime.datetime.strptime(' '.join(last_script_run),'%b %d %H:%M:%S'):
            flag = 1
        flagPassing = 0;
        with open(logfile, 'r') as file:
            for line in file:
                if (flag ==1):
                    ideaLogs.append(getIdeaLog(line, aanonymised, atargetnet, aname))
                else:
                    if line.split()[0:3] == last_script_run and flagPassing == 0:
                        flagPassing = 1
                    elif line.split()[0:3] != last_script_run and flagPassing == 1:
                        flag =1
                        ideaLogs.append(getIdeaLog(line,  aanonymised, atargetnet, aname))
    file = open(last_run_file, 'w')
    file.write(' '.join(last_log_time))
    return ideaLogs
    
if __name__ == "__main__":
    aconfig = read_cfg(DEFAULT_ACONFIG)
    wconfig = read_cfg(aconfig.get('warden', DEFAULT_WCONFIG))
    
    aname = aconfig.get('name', DEFAULT_NAME)
    awin = aconfig.get('awin', DEFAULT_AWIN) * 60
    wconfig['name'] = aname

    asecret = aconfig.get('secret', DEFAULT_SECRET)
    if asecret:
        wconfig['secret'] = asecret

    wclient = Client(**wconfig)   

    aanonymised = aconfig.get('anonymised', DEFAULT_ANONYMISED)
    if aanonymised not in ['no', 'yes', 'omit']:
        wclient.logger.error("Configuration error: anonymised: '%s' - possible typo? use 'no', 'yes' or 'omit'" % aanonymised)
        sys.exit(2)

    atargetnet  = aconfig.get('target_net', DEFAULT_TARGET_NET)
    aanonymised = aanonymised if (atargetnet != DEFAULT_TARGET_NET) or (aanonymised == 'omit') else DEFAULT_ANONYMISED

    events = parseFile(logfile, aanonymised, atargetnet, aname)
    print "=== Sending ==="
    start = time()
    ret = wclient.sendEvents(events)
    if ret:
        wclient.logger.info("%d event(s) successfully delivered." % len(events))
    print "Time: %f" % (time() - start)
