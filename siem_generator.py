#!/usr/bin/env python
#       File name: siem_generator.py
#       Author: Jakub Toczek (PSNC)
#       Date created: 20/11/2017
#       Python Version: 2.7.13
from warden_client import read_cfg
import datetime
from time import sleep
import random
import json
from collections import OrderedDict

DEFAULT_CONFIG = './generator.cfg'
DEFAULT_OUTPUT = './logs.log'
DEFAULT_MAX_PER_TIMESTAMP = 1
DEFAULT_MIN_SLEEP = 0
DEFAULT_MAX_SLEEP = 3
DEFAULT_MAX_LOGS = 10
DEFAULT_SLEEP = 1

def server():

    config = read_cfg(DEFAULT_CONFIG)
    output = config.get('output',DEFAULT_OUTPUT)
    file = open(output, "a+", 1)

    regular_log = config.get('regular')
    anomaly_log = config.get('anomaly')
    real_sleep = config.get('real_sleep',DEFAULT_SLEEP)
    min_sleep = config.get('min_sleep',DEFAULT_MIN_SLEEP)
    max_sleep = config.get('max_sleep',DEFAULT_MAX_SLEEP)
    max_logs = config.get('max_logs',DEFAULT_MAX_LOGS)
    log_count=0
    iterate = True
    date = datetime.datetime.now()
    while (iterate):
        # sleep
        if (real_sleep):
            sleep(random.uniform(min_sleep, max_sleep))
            date = datetime.datetime.now()
        else:
            sleep_time = random.uniform(min_sleep, max_sleep)
            date = date+datetime.timedelta(seconds=sleep_time)
        if (random.randint(1,100) != 1):
            file.write(create_siem_log(regular_log, date))
        else:
            file.write(create_siem_log(anomaly_log, date))
        log_count+=1
        if log_count>=max_logs:
            iterate=False


def create_siem_log(log_type, date):
    date = datetime.datetime.now()
    header = date.strftime('%b %d %H:%M:%S') + " " + random.choice(log_type.get('siem_ip')) + " McAfee_SIEM: "

    body = OrderedDict()
    source_dictionary = OrderedDict()
    source_dictionary.update({'id': random.randint(100000, 999999)})
    source_dictionary.update({'name': random.choice(log_type.get("source_name"))})
    source_dictionary.update({'subnet': random.choice(log_type.get("subnet"))})
    body.update({'source': source_dictionary})
    data_dictionary = OrderedDict()
    data_dictionary.update({'unique_id': random.randint(100000, 999999)})
    data_dictionary.update({'alert_id': random.randint(100000, 999999)})
    data_dictionary.update({'thirdpartytype': random.choice(log_type.get("thirdpartytype"))})

    sig_dictionary = OrderedDict()
    sig_dictionary.update({'id': random.randint(100000, 999999)})
    sig_dictionary.update({'name': random.choice(log_type.get("sig_name"))})
    data_dictionary.update({'sig': sig_dictionary})

    norm_sig_dictionary= OrderedDict()
    norm_sig_dictionary.update({'id': random.randint(100000, 999999)})
    norm_sig_dictionary.update({'name': random.choice(log_type.get("norm_sig_name"))})
    data_dictionary.update({'norm_sig': norm_sig_dictionary})

    data_dictionary.update({'action': random.choice(log_type.get("action"))})
    data_dictionary.update({'src_ip': random.choice(log_type.get("src_ip"))})
    data_dictionary.update({'dst_ip': random.choice(log_type.get("dst_ip"))})
    data_dictionary.update({'src_port': random.choice(log_type.get("src_port"))})
    data_dictionary.update({'dst_port': random.choice(log_type.get("dst_port"))})
    data_dictionary.update({'protocol': random.choice(log_type.get("protocol"))})
    data_dictionary.update({'src_mac': random.choice(log_type.get("src_mac"))})
    data_dictionary.update({'dst_mac': random.choice(log_type.get("dst_mac"))})
    data_dictionary.update({'firsttime': date.strftime('%Y-%m-%dT%H:%M:%SZ')})
    data_dictionary.update({'lasttime': date.strftime('%Y-%m-%dT%H:%M:%SZ')})
    data_dictionary.update({'writetime': date.strftime('%Y-%m-%dT%H:%M:%SZ')})
    data_dictionary.update({'src_guid': random.choice(log_type.get("src_guid"))})
    data_dictionary.update({'dst_guid':random.choice(log_type.get("dst_guid"))})
    data_dictionary.update({'total_severity': random.randint(1,30)})
    data_dictionary.update({'severity': random.randint(1,30)})
    data_dictionary.update({'eventcount': random.choice(log_type.get("eventcount"))})
    data_dictionary.update({'flow': random.choice(log_type.get("flow"))})
    data_dictionary.update({'vlan': random.choice(log_type.get("vlan"))})
    data_dictionary.update({'sequence': random.choice(log_type.get("sequence"))})
    data_dictionary.update({'trusted': random.choice(log_type.get("trusted"))})
    data_dictionary.update({'session_id': random.choice(log_type.get("session_id"))})
    data_dictionary.update({'compression_level': random.choice(log_type.get("compression_level"))})
    data_dictionary.update({'reviewed': random.choice(log_type.get("reviewed"))})

    body.update({'data': data_dictionary})
    return header + str(json.dumps(body)+'\n')

def getKeyRValue(dictionary, key, delimiter):
    return key+delimiter+str(random.choice(dictionary.get(key)))

server()